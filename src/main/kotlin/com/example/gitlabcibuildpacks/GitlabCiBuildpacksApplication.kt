package com.example.gitlabcibuildpacks

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
class GitlabCiBuildpacksApplication

fun main(args: Array<String>) {
	runApplication<GitlabCiBuildpacksApplication>(*args)
}

@RestController
class AppContoller() {

	@GetMapping("/")
	fun index(): String {
		return "Hello, World!"
	}

}
